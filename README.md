<h1 align="center">COP Storehouse</h1>
<h3 align="center">Armazém de instâncias de problemas de otimização combinatória</h3>

#
![license](https://img.shields.io/static/v1?label=License&message=?&color=blue)
![laai](https://img.shields.io/static/v1?label=LAAI&message=2020&color=orangered)

Este repositório tem por objetivo armazenar arquivos de instâncias de problemas de otimização combinatória conhecidas na literatura para utilização na pesquisa de @ronalddpinho, mas não se atendo somente a esta, servindo de referência para trabalhos futuros na mesma linha de pesquisa.

## Bases de Dados

**OR-Library** - http://people.brunel.ac.uk/~mastjjb/jeb/info.html

**DIMACS** - https://turing.cs.hbg.psu.edu/txn131/index.html

## Problemas

* [ ] Cobertura de conjuntos:

* [ ] Mochila multidimensional:

* [ ] Árvore de Steiner:

* [ ] Clique máximo:

## Licença

/* TODO */